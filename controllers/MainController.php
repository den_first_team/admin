<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;

class MainController extends \yii\base\Controller
{
    public function init()
    {
        //   dump(Yii::$app->controller, 1);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function () {
                    return Yii::$app->response->redirect(Url::to(['/admin/login']));
                },
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'except' => ['login'],
            ],
        ];
    }
}
