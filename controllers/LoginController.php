<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\Url;
use app\modules\admin\models\Login;

class LoginController extends MainController
{
    public $layout = 'login';

    public function actionIndex()
    {
        $modelForm = new Login();
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->login()) {
            return Yii::$app->response->redirect(Url::to(['/admin']));
        }

        $modelForm->password = '';

        return $this->render('index', ['model' => $modelForm]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return Yii::$app->response->redirect(Url::to(['/admin/login']));
    }
}
