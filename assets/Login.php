<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Description of AdminLoginAsset.
 *
 * @author programmer_5
 */
class Login extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/adminAssets/css/bootstrap.min.css',
        'web/adminAssets/css/bootstrap-responsive.min.css',
        'web/adminAssets/css/matrix-login.css',
        'web/adminAssets/font-awesome/css/font-awesome.css',
    ];
    public $js = [
        'web/adminAssets/js/matrix.login.js',
        'web/adminAssets/js/jquery.min.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
