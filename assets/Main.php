<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Description of admin.
 *
 * @author programmer_5
 */
class Main extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/adminAssets/css/bootstrap.min.css',
        'web/adminAssets/css/bootstrap-responsive.min.css',
        'web/adminAssets/css/colorpicker.css',
        'web/adminAssets/css/datepicker.css',
        'web/adminAssets/css/uniform.css',
        'web/adminAssets/css/select2.css',
        'web/adminAssets/css/matrix-style.css',
        'web/adminAssets/css/matrix-media.css',
        'web/adminAssets/css/bootstrap-wysihtml5.css',
        'web/adminAssets/font-awesome/css/font-awesome.css',
       'http://fonts.googleapis.com/css?family=Open+Sans:400,700,800',
    ];
    public $js = [
        'web/adminAssets/js/jquery.min.js',
        'web/adminAssets/js/jquery.ui.custom.js',
        'web/adminAssets/js/bootstrap.min.js',
        'web/adminAssets/js/bootstrap-colorpicker.js',
        'web/adminAssets/js/bootstrap-datepicker.js',
        'web/adminAssets/js/masked.js',
        'web/adminAssets/js/jquery.uniform.js',
        'web/adminAssets/js/select2.min.js',
        'web/adminAssets/js/matrix.js',
        'web/adminAssets/js/matrix.form_common.js',
        'web/adminAssets/js/wysihtml5-0.3.0.js',
        'web/adminAssets/js/jquery.peity.min.js',
        'web/adminAssets/js/bootstrap-wysihtml5.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
