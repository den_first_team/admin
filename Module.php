<?php

namespace app\modules\admin;

use Yii;

/**
 * admin module definition class.
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $addRules = [
            'admin/login' => 'admin/default/login',
        ];
        Yii::$app->getUrlManager()->addRules($addRules);
        Yii::$app->errorHandler->errorAction = 'admin/error';
        $this->params['logo'] = '/web/adminAssets/img/logo.png';
    }
}
