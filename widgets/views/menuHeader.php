<?php 

use yii\helpers\ArrayHelper;

?>
<ul class="nav">

    <?php if ($menuList) :?>
    <?php foreach ($menuList as $k => $menu) :?>

    <?php if (isset($menu['subs']) && $menu['subs']) :?>
    <li class="dropdown" id="<?=$k; ?>">
        <a title="<?=ArrayHelper::getValue($menu, 'label', ''); ?>" href="#" data-toggle="dropdown" data-target="#<?=$k; ?>" class="dropdown-toggle">
            <i class="<?=ArrayHelper::getValue($menu, 'icon', ''); ?>"></i>
            <span class="text">
                <?=ArrayHelper::getValue($menu, 'label', $k); ?>
            </span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <?php foreach ($menu['subs'] as $k1 => $menuSubs) :?>
            <li>
                <a href="<?=ArrayHelper::getValue($menuSubs, 'url', '#'); ?>">
                    <i class="<?=ArrayHelper::getValue($menuSubs, 'icon', ''); ?>"></i>
                    <?=ArrayHelper::getValue($menuSubs, 'label', $k1); ?>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    </li>
    <?php else: ?>
    <li class="">
        <a title="<?=ArrayHelper::getValue($menu, 'label', $k); ?>" href="<?=ArrayHelper::getValue($menu, 'url', '#'); ?>">
            <i class="<?=ArrayHelper::getValue($menu, 'icon', ''); ?>"></i>
            <span class="text">
                <?=ArrayHelper::getValue($menu, 'label', $k); ?>
            </span>
        </a>
    </li>
    <?php endif; ?>

    <?php endforeach; ?>
    <?php endif; ?>


</ul>