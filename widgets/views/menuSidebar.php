<div id="sidebar">
    <?php if ($menuList): ?>
    <ul>
        <?php foreach ($menuList as $menuKey => $menuInfo): ?>
        <li class="<?=isset($menuInfo['class']) ? $menuInfo['class'] : ''; ?> <?=isset($menuInfo['subs']) && $menuInfo['subs'] ? 'submenu' : ''; ?>">


            <a href="<?=$menuInfo['url']; ?>">
                <i class="<?=$menuInfo['icon']; ?>"></i>
                <span>
                    <?=$menuInfo['label']; ?>
                </span>
            </a>
            <?php if (isset($menuInfo['subs']) && $menuInfo['subs']) :?>
            <ul style="<?=isset($menuInfo['subsStyle']) ? $menuInfo['subsStyle'] : ''; ?>">
                <?php foreach ($menuInfo['subs'] as $menuSubKey => $menuSubInfo): ?>
                <li class="<?=isset($menuSubInfo['class']) ? $menuSubInfo['class'] : ''; ?>">
                    <a href="<?=$menuSubInfo['url']; ?>">
                        <?=$menuSubInfo['label']; ?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </li>
        <?php endforeach; ?>
    </ul>

    <?php endif; ?>

</div>
