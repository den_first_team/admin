<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\admin\widgets;

use yii\base\Widget;
use yii\helpers\Url;

/**
 * Description of Form.
 *
 * @author programmer_5
 */
class MenuSidebar extends Widget
{
    public $menuList;
    public $addParams;

    public function init()
    {
        parent::init();
        $this->menuList = [
            'test' => [
                'label' => 'Портфолио',
                'icon' => 'icon icon-inbox',
                'url' => Url::to(['admin/portfolio']),
            ],
            'with_subs' => [
                'label' => 'Настройки',
                'icon' => 'icon-google-plus',
                'url' => '#',
                'subs' => [
                    'file' => [
                        'label' => 'Файл на скачку',
                        'url' => Url::to(['admin/settings/file']),
                    ],
                    'contacts' => [
                        'label' => 'Контактные данные',
                        'url' => Url::to(['admin/settings/contacts']),
                    ],
                ],
            ],
        ];
    }

    /**
     * Get the value of menuList.
     */
    public function getMenuList()
    {
        $menuList = $this->_marginMenu($this->menuList, $this->addParams);
        //  dump($menuList, 1);

        return $menuList;
    }

    protected function _marginMenu($menu, $data)
    {
        if (is_array($data) && $data) {
            foreach ($data as $key => $params) {
                $menu[$key] = isset($menu[$key]) ? $menu[$key] + $params : $params;
                if (isset($menu[$key]['subs']) && $menu[$key]['subs'] && isset($params['subs'])) {
                    $menu[$key]['subs'] = $this->_marginMenu($menu[$key]['subs'], $params['subs']);
                }
            }
        }

        return $menu;
    }

    /**
     * Get the value of addParams.
     */
    public function getAddParams()
    {
        return $this->addParams;
    }

    public function run()
    {
        return $this->render('menuSidebar', [
            'menuList' => $this->getMenuList(),
        ]);
    }
}
