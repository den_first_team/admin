<?php

namespace app\modules\admin\widgets;

use yii\base\Widget;
use yii\helpers\Url;

class MenuHeader extends Widget
{
    public $menuList;

    public function init()
    {
        parent::init();
        $this->menuList = [
            // 'with_subs' => [
            //     'label' => 'With subs',
            //     'icon' => 'icon-google-plus',
            //     'url' => '#',
            //     'subs' => [
            //         'sub1' => [
            //             'label' => 'Sub1',
            //             'url' => Url::to(['admin/with_subs/sub1']),
            //             'icon' => 'icon-google-plus',
            //         ],
            //     ],
            // ],
            // 'logout' => [
            //     'label' => 'Logout',
            //     'icon' => 'icon icon-share-alt',
            //     'url' => Url::to(['/admin/login/logout']),
            // ],
        ];
    }

    public function run()
    {
        return $this->render('menuHeader', ['menuList' => $this->menuList]);
    }
}
