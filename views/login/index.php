<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

//use Yii;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';

?>

 <div id="loginbox">  
 <?php $form = ActiveForm::begin([
        'id' => 'loginform',
       // 'class' => 'form-vertical',
        'options' => [
            'class' => 'form-vertical',
        ],
       // 'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{input}<span style='color:#b94a48;' generated='true' class='help-inline'>{error}</span>",
           // 'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>            
          
				<div class="control-group normal_text"> <h3><img src="<?= \Yii::$app->controller->module->params['logo']; ?>" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span>
                            <?= $form->field($model, 'username', ['options' => ['tag' => false], 'errorOptions' => ['tag' => null]])->textInput(['autofocus' => true, 'placeholder' => 'Username', 'class' => '']); ?>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span>
                            <?= $form->field($model, 'password', ['options' => ['tag' => false], 'errorOptions' => ['tag' => null]])->passwordInput(['placeholder' => 'Password', 'class' => '']); ?>
                        </div>
                    </div>
                </div> 
                <div class="form-actions">
                    <!-- <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Lost password?</a></span> -->
                    <span class="pull-right"><button type="submit" class="btn btn-success" /> Login</button></span>
                </div>
                <?php ActiveForm::end(); ?>
            
        </div>


