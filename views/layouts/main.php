<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\modules\admin\assets\Main;

Main::register($this);
$menuSidebar = isset($this->params['menuSidebar']) ? $this->params['menuSidebar'] : null;
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language; ?>">

<head>
  <meta charset="<?=Yii::$app->charset; ?>">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?=Html::csrfMetaTags(); ?>
    <title>
      <?=Html::encode($this->title); ?>
    </title>

    <?php $this->head(); ?>
</head>

<body>
  <?php $this->beginBody(); ?>

  <!--Header-part-->
  <div id="header">
    <h1 style="background: url(<?= \Yii::$app->controller->module->params['logo']; ?>) no-repeat scroll 0 0 transparent;">
      <a href="<?=Url::to(['/admin']); ?>">Admin</a>
    </h1>
  </div>
  <!--close-Header-part-->


  <!--top-Header-menu-->
  <div id="user-nav" class="navbar navbar-inverse">
  
  <?=\app\modules\admin\widgets\MenuHeader::widget(); ?>

  </div>
  <!--close-top-Header-menu-->
  <!--start-top-serch-->
  <div id="search">
    <input type="text" placeholder="Search here..." />
    <button type="submit" class="tip-bottom" title="Search">
      <i class="icon-search icon-white"></i>
    </button>
  </div>
  <!--close-top-serch-->
  <!--sidebar-menu-->
  <?=\app\modules\admin\widgets\MenuSidebar::widget(['addParams' => $menuSidebar]); ?>
    <!--sidebar-menu-->

    <!--main-container-part-->
    <div id="content">
      <div id="content-header">

        <?= Breadcrumbs::widget([
              'options' => ['id' => 'breadcrumb'],
              'tag' => 'div',
              'itemTemplate' => "{link}\n",
              'encodeLabels' => false,
              'homeLink' => [
                'label' => '<i class="icon-home"></i>Home',
                'url' => ['admin'],
                'class' => 'tip-bottom',
              ],
              'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?>

          <!-- <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Interface elements</a>  -->


          <h1>
            <?=isset($this->params['pageHeader']) ? $this->params['pageHeader'] : ''; ?>
          </h1>
      </div>
      <?=$content; ?>
    </div>

    <!--end-main-container-part-->

    <!--Footer-part-->

    <div class="row-fluid">
      <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by
        <a href="http://themedesigner.in">Themedesigner.in</a>
      </div>
    </div>

    <!--end-Footer-part-->

    <?php $this->endBody(); ?>

</body>

</html>

<?php $this->endPage(); ?>





<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage(newURL) {

    // if url is empty, skip the menu dividers and reset the menu selection to default
    if (newURL != "") {

      // if url is "-", it is this page -- reset the menu:
      if (newURL == "-") {
        resetMenu();
      }
      // else, send page to designated URL
      else {
        document.location.href = newURL;
      }
    }
  }

  // resets the menu selection upon entry to this page:
  function resetMenu() {
    document.gomenu.selector.selectedIndex = 2;
  }
</script>