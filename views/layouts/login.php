<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\modules\admin\assets\Login;
use yii\helpers\Html;

Login::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language; ?>">
<head>
<title>Matrix Admin</title><meta charset="UTF-8" />
    <meta charset="<?=Yii::$app->charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?=Html::csrfMetaTags(); ?>
    <title><?=Html::encode($this->title); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<?= $content; ?> 

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage();?>
